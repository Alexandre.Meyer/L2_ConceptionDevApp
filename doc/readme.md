

# Documentations


## Installation des outils de dév sur sa machine

* [Toutes les installations selon votre OS](install.md)
* [WSL](wsl.md) : si vous voulez un Linux sous Windows sans double boot

## IDE

* [IDE en général](ide.md)
* [VSCode](vscode.md)
* [Visual Studio](visualstudio.md) (uniquement sous Windows)
* [XCode](xcode.md) (uniquement sous MacOS)
* [CodeBlocks](codeblocks.md) (Windows et Linux, un peu vieillissant)


## Pour compiler un projet

* [Makefile](makefile.md) (la base)
* [CMake](cmake.md) (l'outil courant qui génère des fichiers de projet comme des Makefile, des projets Visual ou Xcode)


## Gestion du code

* [Quelques règles d'écriture de code](coding_rules.md)
* [Déboguer](debug.md)
* [Documenter son code et produire des documents](doxygen.md)
* [Gestionnaire de version de code (git)](git.md) (indispensable)



## Bibliothèques 

* [STL](stl.md)
* [SDL2](sdl2.md)
* [imgui](imgui.md) ou "Dear ImGUI"


